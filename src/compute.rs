use crate::debug::{draw_dot, draw_line};
use crate::{Error, Image, Point};

const MAX_KERNEL_SIZE: usize = 10;

/// Looking for non alpha pixels on the line, using a threshold:
/// - If a pixel is detected, check if some other pixels around are also opaque
/// - If a pixel is more opaque than the previous one, using this one as "Point"
/// - If no more opaque pixels or end on the line, add the last point a the list
pub(crate) fn detected_entry_points(
    points: &[Point; 2],
    threshold: f32,
    img: &Image,
) -> Result<(Vec<Point>, Image), Error> {
    let mut img_action = img.clone();
    let mut detected_points = vec![];

    let x_min = points[0].x.min(points[1].x);
    let x_max = points[0].x.max(points[1].x);

    let mut current_point_detected: Option<(Point, f32)> = None;

    for x in x_min..x_max {
        let u = (x - x_min) as f32 / x_max as f32;
        let y = (((1_f32 - u) * points[0].y as f32) + (u * points[1].y as f32)).floor() as usize;

        // If a pixel is detected, check if some other pixels around are also opaque
        let current_alpha = (img.get_pixel((x, y)).a as f32) / 255_f32;
        if current_point_detected.is_none() && current_alpha > threshold {
            current_point_detected = Some(((x, y).into(), current_alpha));
            continue;
        }

        // Nothing to do if no point detected, continue
        if current_point_detected.is_none() {
            continue;
        }

        // If a pixel is more opaque than the previous one, using this one as "Point"
        if current_alpha > current_point_detected.unwrap().1 {
            current_point_detected = Some(((x, y).into(), current_alpha));
            continue;
        }

        if current_alpha < threshold {
            detected_points.push(current_point_detected.unwrap().0);
            current_point_detected = None;
        }
    }

    // Draw part: Draw step on the image
    draw_line(
        points[0],
        points[1],
        1,
        &[0_f32, 1_f32, 0_f32],
        &mut img_action,
    )?;

    for point in &detected_points {
        draw_dot(*point, 2, &[1_f32, 0_f32, 0_f32], &mut img_action)?;
    }

    Ok((detected_points, img_action))
}

const INTERSECTION_KERNELS: [[[f32; 3]; 3]; 8] = [
    [
        [0_f32, 1_f32, 0_f32],
        [1_f32, 0_f32, 1_f32],
        [0_f32, 0_f32, 0_f32],
    ],
    [
        [0_f32, 1_f32, 0_f32],
        [0_f32, 0_f32, 1_f32],
        [0_f32, 1_f32, 0_f32],
    ],
    [
        [0_f32, 0_f32, 0_f32],
        [1_f32, 0_f32, 1_f32],
        [0_f32, 1_f32, 0_f32],
    ],
    [
        [0_f32, 1_f32, 0_f32],
        [1_f32, 0_f32, 0_f32],
        [0_f32, 1_f32, 0_f32],
    ],
    [
        [0_f32, 1_f32, 0_f32],
        [0_f32, 0_f32, 0_f32],
        [1_f32, 0_f32, 1_f32],
    ],
    [
        [1_f32, 0_f32, 0_f32],
        [0_f32, 0_f32, 1_f32],
        [1_f32, 0_f32, 0_f32],
    ],
    [
        [1_f32, 0_f32, 1_f32],
        [0_f32, 0_f32, 0_f32],
        [0_f32, 1_f32, 0_f32],
    ],
    [
        [0_f32, 0_f32, 1_f32],
        [1_f32, 0_f32, 0_f32],
        [0_f32, 0_f32, 1_f32],
    ],
];

pub(crate) fn select_line(
    entry_point: impl Into<Point>,
    threshold: f32,
    img: &Image,
) -> Result<Image, Error> {
    let entry_point = entry_point.into();
    let mut img_action = img.clone();

    // Find a kernel size
    let mut kernel_size = 0;
    for size in 1..=MAX_KERNEL_SIZE {
        // No kernel size found, cannot compute line
        if size == MAX_KERNEL_SIZE {
            return Err(Error::Other(
                "Select_line".to_string(),
                "Cannot find line (kernel oversize)".to_string(),
            ));
        }

        let alpha = (img.get_pixel((entry_point.x - size, entry_point.y)).a as f32) / 255_f32;

        // Check left pixels
        if size < entry_point.x && alpha < threshold {
            kernel_size = size * 3;
            break;
        }

        // Check right pixels
        if (size + entry_point.x) < img.width() && alpha < threshold {
            kernel_size = size * 3;
            break;
        }

        // Check top pixels
        if size < entry_point.y && alpha < threshold {
            kernel_size = size * 3;
            break;
        }

        // Check bottom pixels
        if (size + entry_point.y) < img.height() && alpha < threshold {
            kernel_size = size * 3;
            break;
        }
    }

    // A clue is to use the kernels (see on top of the fn) to find intersection

    Ok(img_action)
}
