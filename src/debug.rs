use crate::image::Region;
use crate::{Error, Image, Point};

pub(crate) fn draw_dot(
    point: impl Into<Point>,
    size: usize,
    color: &[f32; 3],
    img: &mut Image,
) -> Result<(), Error> {
    let point = point.into();
    assert!(point.x < img.width());
    assert!(point.y < img.height());
    assert!(size > 0);

    let fx = point.x as f32;
    let fy = point.y as f32;

    let mid_size = size / 2;
    let x = if point.x < mid_size {
        0
    } else {
        point.x - mid_size
    };

    let y = if point.y < mid_size {
        0
    } else {
        point.y - mid_size
    };

    let w = if point.x < mid_size {
        size - mid_size - point.x
    } else {
        size
    };

    let h = if point.y < mid_size {
        size - mid_size - point.y
    } else {
        size
    };

    let rect = Region::new((x, y), (w, h));

    img.for_each_pixel_region_mut(rect, |point, pixel| {
        let px = point.x as f32;
        let py = point.y as f32;

        let mut pix_r = (pixel.r as f32) / 255_f32;
        let mut pix_g = (pixel.g as f32) / 255_f32;
        let mut pix_b = (pixel.b as f32) / 255_f32;
        let mut pix_a = (pixel.a as f32) / 255_f32;

        let distance: f32 = ((fx - px) * (fx - px) + (fy - py) * (fy - py)).sqrt() as f32;
        let alpha = (1_f32 - (distance / (size as f32 / 2_f32))).max(0_f32);

        pix_r = ((1_f32 - alpha) * pix_r) + color[0] * alpha;
        pix_g = ((1_f32 - alpha) * pix_g) + color[1] * alpha;
        pix_b = ((1_f32 - alpha) * pix_b) + color[2] * alpha;
        pix_a = pix_a.max(alpha);

        pixel.r = (pix_r * 255_f32) as u8;
        pixel.g = (pix_g * 255_f32) as u8;
        pixel.b = (pix_b * 255_f32) as u8;
        pixel.a = (pix_a * 255_f32) as u8;
    });

    Ok(())
}

pub(crate) fn draw_line(
    p1: impl Into<Point>,
    p2: impl Into<Point>,
    size: usize,
    color: &[f32; 3],
    img: &mut Image,
) -> Result<(), Error> {
    let p1 = p1.into();
    let p2 = p2.into();
    assert!(p1.x < img.width());
    assert!(p1.y < img.height());
    assert!(p2.x < img.width());
    assert!(p2.y < img.height());
    assert!(size > 0);

    let x_min = p1.x.min(p2.x);
    let x_max = p1.x.max(p2.x);

    for x in x_min..x_max {
        let u = (x - x_min) as f32 / x_max as f32;
        let y = (((1_f32 - u) * p1.y as f32) + (u * p2.y as f32)).floor() as usize;
        draw_dot((x, y), size, color, img)?;
    }

    Ok(())
}
