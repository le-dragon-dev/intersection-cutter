use std::fmt::{Display, Formatter};
use std::fs::File;
use std::path::Path;

const CHANNELS: usize = 4;
pub type ChannelType = u8;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Color {
    pub r: ChannelType,
    pub g: ChannelType,
    pub b: ChannelType,
    pub a: ChannelType,
}

impl Color {
    pub fn new(r: ChannelType, g: ChannelType, b: ChannelType, a: ChannelType) -> Self {
        Self { r, g, b, a }
    }

    pub fn from_rgb(r: ChannelType, g: ChannelType, b: ChannelType) -> Self {
        Self { r, g, b, a: 255 }
    }

    pub fn into_u32(self) -> u32 {
        ((self.r as u32) << 24) + ((self.g as u32) << 16) + ((self.b as u32) << 8) + (self.a as u32)
    }
}

impl From<(u8, u8, u8)> for Color {
    fn from(color: (u8, u8, u8)) -> Self {
        Self {
            r: color.0,
            g: color.1,
            b: color.2,
            a: 255,
        }
    }
}

impl From<(u8, u8, u8, u8)> for Color {
    fn from(color: (u8, u8, u8, u8)) -> Self {
        Self {
            r: color.0,
            g: color.1,
            b: color.2,
            a: color.3,
        }
    }
}

impl From<u32> for Color {
    fn from(color: u32) -> Self {
        Self {
            r: (color >> 24) as ChannelType,
            g: (color >> 16 & 0xFF) as ChannelType,
            b: (color >> 8 & 0xFF) as ChannelType,
            a: (color & 0xFF) as ChannelType,
        }
    }
}

#[derive(Debug, Clone)]
pub enum Error {
    CannotLoadImage(String),
    CannotSaveImage(String),
    Other(String, String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

#[derive(Debug, Copy, Clone)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl From<(usize, usize)> for Point {
    fn from(point: (usize, usize)) -> Self {
        Self {
            x: point.0,
            y: point.1,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Size {
    pub w: usize,
    pub h: usize,
}

impl From<(usize, usize)> for Size {
    fn from(size: (usize, usize)) -> Self {
        Self {
            w: size.0,
            h: size.1,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Region {
    pub position: Point,
    pub size: Size,
}

impl Region {
    pub fn new(pos: impl Into<Point>, size: impl Into<Size>) -> Self {
        Self {
            position: pos.into(),
            size: size.into(),
        }
    }
}

impl From<(usize, usize, usize, usize)> for Region {
    fn from(region: (usize, usize, usize, usize)) -> Self {
        Self {
            position: (region.0, region.1).into(),
            size: (region.2, region.3).into(),
        }
    }
}

#[derive(Clone)]
pub struct Image {
    width: usize,
    height: usize,
    data: Vec<ChannelType>,
}

impl Image {
    pub fn width(&self) -> usize {
        self.width
    }
    pub fn height(&self) -> usize {
        self.height
    }
}

impl Image {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            width,
            height,
            data: vec![0; CHANNELS * width * height],
        }
    }

    pub fn from_png<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let decoder =
            png::Decoder::new(File::open(path).map_err(|e| Error::CannotLoadImage(e.to_string()))?);

        let mut reader = decoder
            .read_info()
            .map_err(|e| Error::CannotLoadImage(e.to_string()))?;

        let mut img = Image::new(reader.info().width as usize, reader.info().height as usize);
        reader
            .next_frame(&mut img.data)
            .map_err(|e| Error::CannotLoadImage(e.to_string()))?;

        Ok(img)
    }

    pub fn save_png<P: AsRef<Path>>(&self, path: P) -> Result<(), Error> {
        let mut encoder = png::Encoder::new(
            File::create(path).map_err(|e| Error::CannotSaveImage(e.to_string()))?,
            self.width as u32,
            self.height as u32,
        );

        encoder.set_color(png::ColorType::Rgba);
        encoder.set_depth(png::BitDepth::Eight);
        encoder.set_trns(vec![0xFF_u8, 0xFF_u8, 0xFF_u8, 0xFF_u8]);

        let mut writer = encoder
            .write_header()
            .map_err(|e| Error::CannotSaveImage(e.to_string()))?;
        writer
            .write_image_data(&self.data)
            .map_err(|e| Error::CannotSaveImage(e.to_string()))?;

        Ok(())
    }

    fn region(&self) -> Region {
        Region {
            position: Point { x: 0, y: 0 },
            size: Size {
                w: self.width,
                h: self.height,
            },
        }
    }

    pub fn get_pixel(&self, point: impl Into<Point>) -> Color {
        let point = point.into();
        assert!(point.x < self.width);
        assert!(point.y < self.height);

        let index = point.x * CHANNELS + point.y * self.width * CHANNELS;
        Color::new(
            self.data[index],
            self.data[index + 1],
            self.data[index + 2],
            self.data[index + 3],
        )
    }

    pub fn for_each_pixel_region<F: Fn(&Point, &Color)>(&self, region: impl Into<Region>, f: F) {
        let region = region.into();
        assert!((region.position.x + region.size.w) < self.width);
        assert!((region.position.y + region.size.h) < self.height);

        for y in region.position.y..region.position.y + region.size.h {
            for x in region.position.x..region.position.x + region.size.w {
                let index = x * CHANNELS + y * self.width * CHANNELS;
                let color: Color = (
                    self.data[index],
                    self.data[index + 1],
                    self.data[index + 2],
                    self.data[index + 3],
                )
                    .into();

                f(&(x, y).into(), &color);
            }
        }
    }

    pub fn for_each_pixel<F: Fn(&Point, &Color)>(&self, f: F) {
        self.for_each_pixel_region(self.region(), f);
    }

    pub fn for_each_pixel_region_mut<F: Fn(&Point, &mut Color)>(
        &mut self,
        region: impl Into<Region>,
        f: F,
    ) {
        let region = region.into();
        assert!((region.position.x + region.size.w) < self.width);
        assert!((region.position.y + region.size.h) < self.height);

        for y in region.position.y..region.position.y + region.size.h {
            for x in region.position.x..region.position.x + region.size.w {
                let index = x * CHANNELS + y * self.width * CHANNELS;
                let mut color: Color = (
                    self.data[index],
                    self.data[index + 1],
                    self.data[index + 2],
                    self.data[index + 3],
                )
                    .into();

                f(&(x, y).into(), &mut color);

                self.data[index] = color.r;
                self.data[index + 1] = color.g;
                self.data[index + 2] = color.b;
                self.data[index + 3] = color.a;
            }
        }
    }

    pub fn for_each_pixel_mut<F: Fn(&Point, &mut Color)>(&mut self, f: F) {
        self.for_each_pixel_region_mut(self.region(), f);
    }
}
