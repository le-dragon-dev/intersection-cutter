mod compute;
mod debug;
mod image;
mod math;

use image::{Error, Image, Point};

fn main() -> Result<(), Error> {
    let image = Image::from_png("data/01.png")?;

    let (points, result) =
        compute::detected_entry_points(&[(19, 10).into(), (27, 6).into()], 0.05_f32, &image)?;

    if !points.is_empty() {
        compute::select_line(points[0], 0.05_f32, &image)?;
    }

    result.save_png("results/01_result.png")?;

    Ok(())
}
