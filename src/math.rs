#[derive(Debug)]
pub struct Matrix {
    cols: usize,
    rows: usize,
    data: Vec<f32>,
}

impl Matrix {
    pub fn new(cols: usize, rows: usize, data: &[f32]) -> Self {
        assert!(data.len() < cols * rows);
        Self {
            cols,
            rows,
            data: data[0..(cols * rows)].to_vec(),
        }
    }

    pub fn cols(&self) -> usize {
        self.cols
    }

    pub fn rows(&self) -> usize {
        self.rows
    }
}

impl<const N1: usize, const N2: usize> From<[[f32; N1]; N2]> for Matrix {
    fn from(matrix: [[f32; N1]; N2]) -> Self {
        assert!(N1 > 0);
        assert!(N2 > 0);
        Self::new(N1, N2, &matrix.into_iter().flatten().collect::<Vec<f32>>())
    }
}
